package br.edu.up;

import java.util.Map;
import java.util.Random;

import com.firebase.client.Firebase;

public class FirebaseUtil {
  public static void writeToList(String url, Map<String, Object> map) {
      long num = new Random().nextLong();
      Firebase listRef = new Firebase(url + "/data/");
      map.put("_id", num);
      Firebase push = listRef.child("" + num); // or .child(Long.toString(num));
      push.setValue(map);
  }
 }
